(defpackage datetime
  (:use cl)
  (:export
   :datetime

   :year
   :month
   :day
   :hour
   :minute
   :second
   :day-of-week

   :month-string
   :day-of-week-string

   :new
   :now
   :decoded-time-to-datetime

   :short-name))

(in-package datetime)

(defvar *day-names*
  '("Monday" "Tuesday" "Wednesday"
    "Thursday" "Friday" "Saturday"
    "Sunday"))

(defvar *month-names*
  '("January" "February" "March"
    "April" "May" "June" "July"
    "August" "September" "October"
    "November" "December"))

(defclass datetime ()
  ((year
    :initarg :year
    :initform 1970
    :accessor datetime-year)
   (month
    :initarg :month
    :initform 1
    :accessor datetime-month)
   (day
    :initarg :day
    :initform 1
    :accessor datetime-day)
   (hour
    :initarg :hour
    :initform 0
    :accessor datetime-hour)
   (minute
    :initarg :minute
    :initform 0
    :accessor datetime-minute)
   (second
    :initarg :second
    :initform 0
    :accessor datetime-second)
   (day-of-week
    :initarg :day-of-week
    :initform 3 ;; Thursday
    :accessor datetime-day-of-week)))

(defgeneric month-string (obj)
  (:documentation "Will give string value of month number. Example: January"))
(defgeneric day-of-week-string (obj)
  (:documentation "Will give string value of day of week number. Example: Monday"))

(defmethod month-string ((obj datetime))
  "Converts and returns the string value of a
   month number."
  (nth (- (datetime-month obj) 1) *month-names*))
(defmethod day-of-week-string ((obj datetime))
  "Converts and returns the string value of a
   day-of-week number."
  (nth (datetime-day-of-week obj) *day-names*))

(defun compute-day-of-week (year month day)
  "Returns the day of the week as an integer.
   Monday is 0."
  (nth-value
   6
   (decode-universal-time
    (encode-universal-time 0 0 0 day month year 0)
    0)))

(defun new (&key year month day hour minute second)
  "Create a new datetime instance.
   Year is four digits. ex: 2018.
   Month is from 1 to 12.
   Day starts at 1 to (28-31)
   Hour is from 0 to 23.
   Minute is from 0 to 59.
   Second is from 0 to 59."
  (make-instance 'datetime
                 :year year
                 :month month
                 :day day
                 :hour hour
                 :minute minute
                 :second second
                 :day-of-week (compute-day-of-week year month day)))

(defmacro decoded-time-to-datetime (decoded-time)
  "Converts a traditional decoded universal time
   to a datetime instance."
  `(multiple-value-bind
        (second minute hour day month year day-of-week)
      ,decoded-time
    (make-instance 'datetime
                   :year year
                   :month month
                   :day day
                   :hour hour
                   :minute minute
                   :second second
                   :day-of-week day-of-week)))

(defun now ()
  "Creates a datetime instance with the current time"
  (decoded-time-to-datetime (get-decoded-time)))

(defun short-name (string &optional (append "."))
  "Shortens a string to the first three characters
   and appends a string to the end of it."
  (concatenate 'string (subseq string 0 3) append))
