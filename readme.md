goalguy.lisp
---

A small program that adds gamification to your daily goals.

It's purpose is to help you build positive habits.

## To run

Load `goalguy.lisp` in your Common Lisp of choice.

## Example Usage

```common-lisp
;;; Initialize goal-hitter.
;;; :save-directory and :filename are optional.
(incremental my-self-improvement
             :save-directory "~/Dropbox/"
             :filename "goalhitterdatabase.lisp")

;;; Optionally create your own point system
(defpointsystem karma-points
  :daily-goal 20
  :daily-bonus 20 ;; If you hit your goal, you get 20 extra points
  :milestones
  '(:egg 0
    :tagpole 100
    :frog 1000
    :frogman 10000))

;;; SCENERIO 1 - BASIC GOAL AND TARGET

(defgoal run-a-marathon
  :start-date "1/1/2018"
  :end-date "3/1/2018" ;;; Optional end date
  ;;; Point prizes you get for completing various objectives.
  :prizes
  '(:made-it-half-way 1000
    :completed-race 10000
    :came-in-first 100000))

(deftraining morning-jog
  :frequency 'daily
  :reward 20   ;;;  20 points per succesful day!
  :penalty -20 ;;; -20 points per day missed.
  )

;;; SCENERIO 2 - SUB-GOALS
;;; When specifying subgoals, you could either
;;; create the goals inline or reference them
;;; as a symbol.

(defgoal get-fit
  :start-date "1/1/2018"
  :end-date "3/1/2018" ;;; Optional end date
  :prizes
  '(:made-it-30-days 50
    :built-a-habit 1000)
  :subgoals
  'run-a-marathon
  (defgoal do-100-pushups-in-one-sitting
    :prizes
    '(:success 1000)))
```
