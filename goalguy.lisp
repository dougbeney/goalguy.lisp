;;; Load the datetime library
(load "datetime.lisp")

;;; Test the datetime library
(defvar *christmas*
  (datetime:new
   :month 12
   :day 25
   :year 2018))

;;; Print the following template:
;;;
;;; Christmas is on a beautiful [DAY OF WEEK].
;;; Christmas date: [MONTH]/[DAY]/[YEAR]
;;;
(format t
        "~%~%Christmas is on a beautiful ~a.~%Christmas date: ~a/~a/~a~%"
        (datetime:day-of-week-string *christmas*)
        (slot-value *christmas* 'datetime:month)
        (slot-value *christmas* 'datetime:day)
        (slot-value *christmas* 'datetime:year))
